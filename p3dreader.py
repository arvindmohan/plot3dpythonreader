"""
Python reader for Plot3d Fortran Unformatted Files
by Arvind Mohan
Code unformatierten Fortran-Dateien mit Python lesen
von Arvind Mohan
See README for details.
"""
import numpy as np
import fortranfile as fofi
from itertools import *


"""  Function definitions """

def extract_variable(data,size,position):
   """ Extracts variables from plot3d files """
   temp = np.empty((xsize,ysize,zsize))   
   it_no = 0
   for i in xrange(xsize):
      for j in xrange(ysize):
         for k in xrange(zsize):
              temp[i,j,k] = data[position*size + it_no]
              if it_no < size:
                 it_no = it_no + 1 
   return temp       
  
 
def get_variables():
  """ Stores data in variables"""
  varname = [0]*num_var
  for i in xrange(num_var):
    varname[i] = extract_variable(data,vecsize,i) 
  return varname  

  
def output_asciifile(name):
  """ outputs unformatted data to ASCII file """
  fhandle = file(name,'w')
  np.savetxt(fhandle,(header),fmt='%d',newline=" ")
  fhandle.write('\n')
  np.savetxt(fhandle,(data),fmt='%e',newline=" ")
  fhandle.close()
  print 'Written file',name  

def  read_file(filename):
  f = fofi.FortranFile(filename,endian='>',header_prec='i')
  global header
  global data
  global num_var
  global vecsize
  global xsize
  global ysize
  global zsize
  header = f.readInts()
  data = f.readReals('f')
  xsize = header[0]
  ysize = header[1]
  zsize = header[2]
  num_var = header[3]
  vecsize = xsize*ysize*zsize
  
def  read_plot3d(filename,i):
  print 'Reading file ... '
  read_file(filename)
  print  'Extracting individual variables ...'
  vars = get_variables()
  if  i == 1:
      output_name = raw_input('Enter a file name for ASCII output: ')
      output_asciifile(output_name)
  return (vars,xsize,ysize,zsize,vecsize)


  




